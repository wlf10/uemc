<div class="row">
  <div class="col-xs-6 col-md-4">
    <span><h3>Форма обратной связи</h3></span>
    <form action="feedback.php" method="POST">
      <div class="form-group">
        <input type="text" class="form-control" required="required" placeholder="Имя" name="name">
      </div>
      <div class="form-group">
        <input type="text" class="form-control" required="required" placeholder="e-mail" name="email">
      </div>
      <div class="form-group">
        <textarea class="form-control" required="required" placeholder="Текст" name="text"></textarea>
      </div>
      <button type="submit" class="btn btn-primary btn-block">Отправить</button>
    </form>
  </div>
  <div class="col-md-6">
  </div>
</div>
