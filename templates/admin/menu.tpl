
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Menu</h1>
    </div>
    <!-- /.col-lg-12 -->

	<div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Main menu
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Dispatch</th>
                                <th>Url</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        	{foreach $menu as $item}
                            <tr>
                                <td>{$item.id}</td>
                                <td>{$item.name}</td>
                                <td>{$item.dispatch}</td>
                                <td>{$item.url}</td>
                                <td class="text-center">
                                	<span class="fa fa-pencil"></span>
                                	<span class="fa fa-trash-o"></span>
                                </td>
                            </tr>
                            {/foreach}
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-6 -->

</div>
<!-- /.row -->