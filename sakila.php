<?php

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

$link = mysqli_connect(
			'localhost', 
			'root', 
			'password',
			'sakila'
		);

if (mysqli_connect_errno()) {
    printf("Ошибка соединения: %s\n", mysqli_connect_error());
    exit();
}

$result = mysqli_query($link, 
	'SELECT * FROM film;');

if ($result) {
    
    /* Выборка результатов запроса */
    while( $row = mysqli_fetch_assoc($result) ){
    	echo $row['title'] . " = " . $row['replacement_cost'] . "<br>";
    }

    /* Освобождаем используемую память */
    mysqli_free_result($result); 
}

/* Закрываем соединение */
mysqli_close($link); 
