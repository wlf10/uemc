<?php

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

require 'libs/Smarty.class.php';
$smarty = new Smarty();

$request = $_GET;

//$smarty->debugging = true;
//$smarty->caching = true;
//$smarty->cache_lifetime = 120;

if (!empty($request['dispath'])) {
	$dispath = $request['dispath'];
} else {
	$dispath = 'index';
}

$link = mysqli_connect(
			'localhost', 
			'root', 
			'password',
			'mysite'
		);

if (mysqli_connect_errno()) {
    echo "Ошибка соединения: " . mysqli_connect_error();
    exit();
}

$result = mysqli_query($link, 
	'SELECT menu_id as id, ' 
	. 'menu_name as name, '
	. 'menu_dispatch as dispatch, ' 
	. 'menu_url as url '
	. 'FROM menu;');

$menu = array();

if (!$result) {
	echo("Error description: " . mysqli_error($link));
    die();
}

if ($result) {

    /* Выборка результатов запроса */
    while($row = mysqli_fetch_assoc($result) ){
    	$id = $row['id'];

        $menu[$id] = $row;
        $menu[$id]['active'] = $row['dispatch'] == $dispath;
        /*if ($row['dispatch'] == $dispath) {
        	$menu[$id]['active'] = true
        } else {
        	$menu[$id]['active'] = false
        }*/

    }

    /* Освобождаем используемую память */
    mysqli_free_result($result); 
}

/*$menu = array(
	'0' => array(
		'name' => 'Main',
		'url' => 'index.php?dispath=index',
		'active' => $dispath == 'index',
	),
	'1' => array(
		'name' => 'Feedback',
		'url' => 'index.php?dispath=feedback',
		'active' => $dispath == 'feedback',
	),
);*/

$smarty->assign("menu", $menu);
$smarty->assign("dispath", $dispath);

$smarty->display("base.tpl");

?>