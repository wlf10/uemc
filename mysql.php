<?php

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

require 'libs/Smarty.class.php';
$smarty = new Smarty();

$link = mysqli_connect(
			'localhost', 
			'root', 
			'password',
			'sakila'
		);

if (mysqli_connect_errno()) {
    echo "Ошибка соединения: " . mysqli_connect_error();
    exit();
}

$result = mysqli_query($link, 'SELECT * FROM film;');

if ($result) {
    $films = array();
    
    /* Выборка результатов запроса */
    while( $row = mysqli_fetch_assoc($result) ){
        $films[$row['film_id']] = $row;
    }

    $smarty->assign("films", $films);

    /* Освобождаем используемую память */
    mysqli_free_result($result); 
}

$smarty->display("mysql.tpl");


/* Закрываем соединение */
mysqli_close($link); 